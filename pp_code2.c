/*  The function should return the length of data which is between two y's. Example: y=0xe, arrray = { 0x1,x2,0xe,0x2,0x3,0x4,0xe). Return 3.  Return -1 if the pattern is not found; */ 


#include <stdio.h>  

int find(char *a, int len, char y) { 
	
	if(a == NULL)
	{
		return -1;
	}
	int start=-1, end=-1, i;
	for(i=0; i<len; i++)
	{
		if(a[i]==y)
		{
			if(start == -1)
			{
				start = i;
			}
			else
			{
				end = i;
				return (end-start-1);
			}
		}




		/*
		if(*(a+i) == y && start == -1)
		{
			start = i;
		}
		else if(*(a+i) == y && start != -1)
		{
			end = i;
			//if( start != -1 && end != -1)
			//{
			return (end-start-1);
			//}
		}
		*/
	}

	return -1;
 } 


 unsigned char find_byte_at_bit_pos(unsigned char *a, int position)
 {
 	unsigned char buffer[2];
 	int iter_char = position/8;
 	int bit_pos = position % 8;
 	
 	buffer[1] = a[iter_char];
 	buffer[0] = a[iter_char+1];
 	printf("%d %d\n",buffer[0],buffer[1]);
 	unsigned short *p = (unsigned short*)buffer;
 	printf("%x\n", *p);
 	*p = (*p) << bit_pos;
 	printf("%x\n", *p);

 	printf("%d %d\n",buffer[0],buffer[1]);

 	return buffer[1];
 	
 }


 int find_bits(unsigned char *a, unsigned char y, int length)
 {
 	unsigned char buffer[2];
 	int i,j;
 	unsigned short *p;
 	int start=-1, end=-1;

 	for (i=0; i<length-1; i++)
 	{
 		buffer[1] = a[i];
 		buffer[0] = a[i+1];
 		p = (unsigned short*)buffer;
 		for(j=0;j<=8;j++)
 		{
 			*p = (*p) << 1;
 			if(buffer[1] == y)
 			{
 				if(start == -1)
 				{
 					start = 8*i+j;
 				}
 				else
 				{
 					end = 8*i+j;
 					return (end - start -8);
 				}
 			}
 		}
 	}
 	return -1;
 }




 int main() {

 	char y = 0xe;
 	unsigned char arr1[] = { 0x8,0x90,0x0,0x1,0x10,0x5,0x2,0xe,0x4};
 	//unsigned char arr1[] = { 0x8,0x90,0x0,0x0,0x2,0xe,0x5,0x2,0x1,0x10};
 	//unsigned char arr1[] = { 0x8,0x90,0x0,0x0,0x2,0xe,0x5,0x2,0xe,0x11};
 	int len = sizeof(arr1)/sizeof(arr1[0]);
 	//int result = find(arr1, len, y);
 	//unsigned char result = find_byte_at_bit_pos(arr1, 49);

 	int result = find_bits(arr1, 0x11, len);

 	printf("%d\n", result);

return 1;
}