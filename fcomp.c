#include<stdio.h>
#include<time.h>
#include<stdlib.h>
#include<string.h>

#define BUFFER_SIZE 4096

void compareFiles(FILE *file1, FILE *file2);

int main(int count_arguments, char *arg[])
{
	FILE *file1, *file2;

	file1 = fopen(arg[1], "rb");
	file2 = fopen(arg[2], "rb");
	if(file1 == NULL || file2 == NULL)
	{
		printf("Can't Open file");
		return 0;
	}

	if(file1 != NULL && file2 != NULL)
	{
		compareFiles(file1, file2);
	}

	return 1;
}

/*
Note : I am assuming that both bin files contain some values to compare.
	   I am not handling those cases where one file is empty.
*/

void compareFiles(FILE *file1, FILE *file2)
{
	clock_t startTimestamp, endTimestamp;
	double timeTaken;
	size_t readByteFile1, readByteFile2;
	// Allocate buffer1 and buffer2 to store values from file1 and file2 respectively.
	void *buffer1 = (void*)calloc(BUFFER_SIZE,sizeof(char));
	void *buffer2 = (void*)calloc(BUFFER_SIZE,sizeof(char));
	int i, j, byteOffset = 0;
	char diff_byte_file1[17], diff_byte_file2[17];
	startTimestamp = clock(); //Starting the clock before reading file and comparison.
	while((readByteFile1 = fread(buffer1, 1, BUFFER_SIZE, file1) > 0) && (readByteFile2 = fread(buffer2, 1, BUFFER_SIZE, file2) > 0))
	{
		long *num_file1 = (long*)buffer1; //Converting buffer1 and buffer2 of long type for comparison.
		long *num_file2 = (long*)buffer2;

		for(i=0; i<(BUFFER_SIZE/sizeof(long)); i++)
		{
			if(((*num_file1) ^ (*num_file2)) != 0)
			{
				char *charFile1 = (char*)num_file1;
				char *charfile2 = (char*)num_file2;
				while(1)
				{
					if(*charFile1 != *charfile2)
					{
						for(j=0; j<16; j++)
						{
							diff_byte_file1[j] = *charFile1;
							diff_byte_file2[j] = *charfile2;
							charFile1++;
							charfile2++; 
						}
						diff_byte_file1[16] = '\0';
						diff_byte_file2[16] = '\0';
						endTimestamp = clock();
						timeTaken = ((double) (endTimestamp - startTimestamp)) / CLOCKS_PER_SEC;
						printf("*** Binary File Comparator ***\n");
						printf("Time_To_Compare: %f ms\n", timeTaken*1000);
						printf("Byte_Offset: %x\n", byteOffset);
						printf("16_Bytes:\n");
						printf("File1:%s File2:%s", diff_byte_file1, diff_byte_file2);
						return;
					}
					byteOffset += sizeof(char);
					charFile1++;
					charfile2++;
				}
			}
			num_file1+=1;
			num_file2+=1;
			byteOffset += sizeof(long);
		}
		memset(buffer1, 0, BUFFER_SIZE);
		memset(buffer2, 0, BUFFER_SIZE);
	}
	endTimestamp = clock();
	timeTaken = ((double) (endTimestamp - startTimestamp)) / CLOCKS_PER_SEC;
	printf("*** Binary File Comparator ***\n");
	printf("Time_To_Compare: %f ms\n", timeTaken*1000);
	free(buffer1);  //Free dynamically allocated buffer
	free(buffer2);  //Free dynamically allocated buffer
	return;
}

/*

------The results will be seen like this when files are not equal: ------------------

*** Binary File Comparator ***
Time_To_Compare: 0.054000 ms
Byte_Offset: 4bec
16_Bytes:
File1:1001100dfghrtydj File2:001100dwbfeiubuw

-------------------------------------------------------------------------------------

The results will be seen like this when files are equal: --------------------------------

*** Binary File Comparator ***
Time_To_Compare: 0.011000 ms

-------------------------------------------------------------------------------------

*/
