#include <stdio.h> 


/*  * Given a byte array(data) of length(len) search for the starting position     * of 32 bit word 0xDEADBEEF and return the position  * If it is not found return -1 */ 


int search_sync(unsigned char *data, int len) {    

	int i, j;
	for(i = 0; i<=len-4; i++)
	{
		//printf("%d", *(data+i));
		if(*(data+i) == 0xde)
		{
			//printf("check");
			for(j=1; j<4; j++)
			{
				//printf("check1");
				if(j==1 && *(data+i+j) == 0xad)
				{
					continue;
				}
				else if(j==2 && *(data+i+j) == 0xbe)
				{
					continue;
				}
				else if(j==3 && *(data+i+j) == 0xef)
				{
					return (i);
				}
				else
				{
					break;
				}
			}
		}	
	}

	
return -1;     

} 
	
unsigned char d1[] = {   0xde,   0xad,  0xbe,   0xef, 0, 0, 12, 34, 56, 78 };  
unsigned char d2[] = {   12,   34,  56,   78, 0xde, 0xad, 0xbe, 0xe0, 0, 0 };


int main(void)  { 

	int pos = -1;     
	printf("In Main1\n"); 
	pos = search_sync(d1,10); 
	printf("pos=%d\n",pos);
	pos = search_sync(d1,8);
	printf("pos=%d\n",pos);
	pos = search_sync(d2,10);     
	printf("pos=%d\n",pos); 

return 0;
} 